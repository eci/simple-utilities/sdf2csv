# Convert SDF Files to MetFrag-compatible CSVs

Two different schemes to convert SDF files to Metfrag compatible CSV files are described in this repository

## 1) Guile Scheme

### Usage Instructions

1.  Adapt the code with new entries and mapping based on the examples in the code itself.
2.  Adapt paths at the bottom of `sdf2csv.scm`.
3.  Run with `guile sdf2csv.scm`.

### Future

The script should be converted into a command line utility which runs like `sdf2csv.scm sdf-config.yaml input.sdf output.csv`.

### Requirements

A **Guile Scheme** interpreter, version \>2.2, see [GNU website](https://www.gnu.org/software/guile).

## 2) R Scheme, LIPIDMAPS focused

### Usage Instructions

1.  Download the latest SDF file in the [LIPIDMAPS website](https://www.lipidmaps.org/databases/lmsd/download).
2.  Run with `LIPIDMAPS_SDF2CSV.R`.

### LIPIDMAPS CSV formated available in the Zenodo repository

The output file, CSV formatted, can be found [here](https://zenodo.org/record/8144127).
