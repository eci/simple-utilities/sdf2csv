(use-modules (ice-9 rdelim))
(use-modules (ice-9 regex))
(use-modules (ice-9 vlist))
(use-modules (ice-9 textual-ports))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-1))
(use-modules (rnrs io ports))



(define-record-type <entry>
  (make-entry name value)
  entry?
  (name entry-name)
  (value entry-value))


(define (make-compound-entry) '())
(define (compound-entry-add entry key val)
  (cons  `(,key . ,val) entry))

(define (compound-entry-get entry key)
  (assoc-ref entry key))



;; Macro defining a specialised compound record type that holds only
;; the required SDF entry fields. Waaay overingineered, but a really
;; cool definition.
(define-syntax define-compound-record
  (lambda (x)
    (syntax-case x ()
      ((_ record-name entry entry* ...)
       (let* ((rn (syntax->datum #'record-name))
	      (name-rn (datum->syntax x (symbol-append '< rn '>)))
	      (entries (map-in-order (lambda (y) (syntax->datum y)) #'(entry entry* ...)))
	      (rec-entries (map-in-order (lambda (y) (datum->syntax x `(,y ,(symbol-append 'get- rn '- y)))) entries))
	      (fun-entries (datum->syntax x (map-in-order (lambda (y) (symbol-append 'get- rn '- y))
							  entries)))
	      (record-constructor (datum->syntax x
						 (symbol-append 'make- rn)))
	      (record? (datum->syntax x (symbol-append rn '?)))
	      (get-fun (datum->syntax x (symbol-append 'get- rn))))
	 (syntax-case fun-entries ()
	   ((fe fe* ...)
	    #`(begin
		(define-record-type #,name-rn (#,record-constructor entry entry* ...)
		  #,record?
		  #,@rec-entries)
		(define #,get-fun (lambda (rec e)
				    (let* ((legend `((entry . ,fe) (entry* . ,fe*) ...))
					   (getter (assoc-ref legend e)))
				      (if getter (getter rec) (error (format #f "Unknown entry ~a" e))))))))))))))




(define (symbol-append . args)
  (string->symbol (apply string-append
			 (map-in-order (lambda (x) (symbol->string x))
				       args))))



(define ENTRY-REGEXP (make-regexp "^> {1}<([^>]+)>\n(.*)" regexp/newline))

(define END-COMPOUND (make-regexp "^\\${4}"))

(define END-STRUCTURE (make-regexp "^M {2}END"))

(define (end-crawler p end-rxp)
  (let loop ((line (read-line p 'concat))
	     (res ""))
    (if (not (eof-object? line))
	(if (not (regexp-exec end-rxp line))
	    (loop (read-line p 'concat)
		  (string-append res line))
	    res)
	(eof-object))))

(define (get-compound-text p)
  (end-crawler p END-COMPOUND))

(define (get-structure-text p)
  (end-crawler p END-STRUCTURE))

(define (get-header-text p)
  (let ((1st (read-line p 'concat)))
    (if (eof-object? 1st)
	(eof-object)
	(string-append 1st
		       (read-line p 'concat)
		       (read-line p)))))

(define (get-entry-text p)
  (get-header-text p))


(define (text->entry entry-text)
  (if (not (eof-object? entry-text))
      (let*
	  ((matches (regexp-exec ENTRY-REGEXP
				 entry-text))
	   (entry-name (match:substring matches 1))
	   (match-value (match:substring matches 2))
	   (entry-value (if match-value match-value "")))
	(make-entry entry-name entry-value))
      'end-of-compound))

(define (p->entry p)
  (text->entry
   (get-entry-text p)))

(define (p->all-entries p)
  (let
      ((text (get-compound-text p)))
    (call-with-input-string text
      (lambda (p)
	(let
	    ((header (get-header-text p))
	     (structure (get-structure-text p)))
	  (let loop
	      ((entry (p->entry p))
	       (res '()))
	    (if (not (eq? 'end-of-compound entry))
		(loop (p->entry p)
		      (cons entry res))
		(map (lambda (x) (entry-name x)) res))))))))





(define (p->compound p entries)
  (let
      ((text (get-compound-text p)))
    (if (not (eof-object? text))
	(call-with-input-string text
	  (lambda (p)
	    (let
		((header (get-header-text p))
		 (structure (get-structure-text p)))
	      (let loop
	      	  ((entry (p->entry p))
	      	   (remaining entries)
	      	   (cmpd-entry (make-compound-entry)))
	      	(if (not (eq? entry 'end-of-compound))
	      	    (let* ((ename (entry-name entry))
	      	    	   (new-rem (delete ename remaining))
	      	    	   (in? (if (eqv? (length remaining)
	      	    			  (+ 1 (length new-rem)))
	      	    		    #t #f))
	      	    	   (res (if in?
	      	    		    (compound-entry-add cmpd-entry ename
	      	    					(entry-value entry))
	      	    		    cmpd-entry)))
	      	      (loop (p->entry p)
	      	    	    new-rem
	      	    	    res))
	      	    cmpd-entry)))))
	(eof-object))))








(define* (convert-file fn-in fn-out #:key entries header-fun convert-fun)
  (call-with-output-file fn-out (lambda (p-out)
				  (header-fun p-out)
				  (call-with-input-file fn-in (lambda (p-in)
								(let loop
								    ((compound (p->compound p-in entries)))
								  (if (not (eof-object? compound))
								      (begin (convert-fun p-out compound)
									     (loop (p->compound p-in entries)))
								      #f)))))))





;; This also defines the order of fields in a record.
(define ymdb-entries
  '("YMDB_ID"
    "GENERIC_NAME"
    "FORMULA"
    "INCHI_KEY"
    "INCHI_IDENTIFIER"
    "EXACT_MASS"
    "SMILES"
    "JCHEM_POLAR_SURFACE_AREA"
    "JCHEM_PKA"
    "JCHEM_PKA_STRONGEST_BASIC"
    "JCHEM_PKA_STRONGEST_ACIDIC"
    "JCHEM_TRADITIONAL_IUPAC"
    "JCHEM_PHYSIOLOGICAL_CHARGE"
    "JCHEM_FORMAL_CHARGE"
    "JCHEM_DONOR_COUNT"
    "JCHEM_ACCEPTOR_COUNT"
    "JCHEM_LOGP"
    "ALOGPS_LOGS"
    "ALOGPS_LOGP"
    "DATABASE_NAME"
    "DATABASE_ID"))

(define hmdb-entries
  '("DATABASE_ID"
    "GENERIC_NAME"
    "FORMULA"
    "INCHI_KEY"
    "INCHI_IDENTIFIER"
    "EXACT_MASS"
    "SMILES"
    "ALOGPS_LOGP"))



(define* (entry-ymdb->csv p entry keys #:key (sep ","))
  (put-string p (string-append (string-join
				(map-in-order (lambda (k)
						(let* ((val (compound-entry-get entry k))
						       (val2 (if val val "")))
						  val2))
					      keys)
				sep)
			       "\n")))

(define* (entry->csv p entry keys #:key (sep ","))
  (put-string p (string-append (string-join
				(map-in-order (lambda (k)
						(let* ((val (compound-entry-get entry k))
						       (val2 (if val val "")))
						  val2))
					      keys)
				sep)
			       "\n")))

(define ymdb-metfrag-map-short
  '(("YMDB_ID" . "Identifier")
    ("GENERIC_NAME" . "CompoundName")
    ("FORMULA" . "MolecularFormula")
    ("INCHI_KEY" . "InChIkey")
    ("INCHI_IDENTIFIER" . "InChI")
    ("EXACT_MASS" . "MonoisotopicMass")
    ("SMILES" . "SMILES")))

(define hmdb-metfrag-map-short
  '(("DATABASE_ID" . "Identifier")
    ("GENERIC_NAME" . "CompoundName")
    ("FORMULA" . "MolecularFormula")
    ("INCHI_KEY" . "InChIkey")
    ("INCHI_IDENTIFIER" . "InChI")
    ("EXACT_MASS" . "MonoisotopicMass")
    ("SMILES" . "SMILES")
    ("ALOGPS_LOGP" . "XLogP")))

(define ymdb-metfrag-map
  (let*
      ((keys-short (map (lambda (e) (car e)) ymdb-metfrag-map-short))
       (filtered (lset-difference equal? ymdb-entries keys-short))
       (dupl (map (lambda (x) `(,x . ,x)) filtered)))
    (append ymdb-metfrag-map-short dupl)))

(define (db-metfrag-map entries metfrag-map)
  (let*
      ((keys-short (map (lambda (e) (car e)) metfrag-map))
       (filtered (lset-difference equal? entries keys-short))
       (dupl (map (lambda (x) `(,x . ,x)) filtered)))
    (append metfrag-map dupl)))

(define hmdb-metfrag-map (db-metfrag-map hmdb-entries hmdb-metfrag-map-short))

(define (ymdb-header p)
  (put-string
   p
   (string-append (string-join (map-in-order (lambda (x) (cdr x))
					     ymdb-metfrag-map)
			       ",")
		  "\n")))

(define (hmdb-header p)
  (put-string
   p
   (string-append (string-join (map-in-order (lambda (x) (cdr x))
					     hmdb-metfrag-map)
			       ",")
		  "\n")))


;; (convert-file "/home/todor/Downloads/ymdb.sdf" "/home/todor/Downloads/ymdb.csv"
;; 	      #:entries ymdb-entries #:header-fun ymdb-header
;; 	      #:convert-fun (lambda (p entry) (entry-ymdb->csv p entry ymdb-entries)))

(convert-file "csf_metabolites_structures.sdf" "csf_metabolites_structures.csv"
	      #:entries hmdb-entries #:header-fun hmdb-header
	      #:convert-fun (lambda (p entry) (entry->csv p entry hmdb-entries)))
